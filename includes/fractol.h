/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/02 12:38:42 by fldoucet          #+#    #+#             */
/*   Updated: 2019/08/14 13:29:50 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# define X_MAX 1000
# define Y_MAX 1000

# include <math.h>
# include <pthread.h>
# include "../libft/include/libft.h"
# include "../minilibx/minilibx_linux/includes/mlx.h"

typedef struct	s_fractol
{
	int		it_max;
	int		smooth;
	int		stop;
	int		mouse_x;
	int		mouse_y;
	int		clr;
	int		boost;
	int		y;
	int		y_max;
	char	*fractale;
	void	*mlx_ptr;
	void	*win_ptr;
	void	*img_ptr;
	void	*data_ptr;
	double	tmp;
	double	z_r;
	double	z_i;
	double	c_r;
	double	c_i;
	double	x1;
	double	x2;
	double	y1;
	double	y2;
	double	zoom;
	int		*pixels_r;
	int		*pixels_g;
	int		*pixels_b;
}				t_fractol;

typedef struct	s_pixel
{
	int		x;
	int		y;
}				t_pixel;

void			mandelbrot(t_fractol *fractol);
void			mandelbrot_pthread(t_fractol *fractol);
void			julia(t_fractol *fractol);
void			julia_pthread(t_fractol *fractol);
void			smooth_move_julia(int key, t_fractol *fractol);
void			chameleon(t_fractol *fractol);
void			chameleon_pthread(t_fractol *fractol);
void			c_mandelbrot(t_fractol *fractol);
void			c_mandelbrot_pthread(t_fractol *fractol);
void			burningship(t_fractol *fractol);
void			burningship_pthread(t_fractol *fractol);
void			buddhabrot(t_fractol *fractol);
int				ft_deal_key(int key, t_fractol *fractol);
int				give_color(int i);
int				get_color(double i, t_fractol *fractol);
int				handle_mouse(int key, int x, int y, t_fractol *fractol);
int				handle_mouse_pos(int x, int y, t_fractol *fractol);
void			draw_fractale(t_fractol *fractol);
void			handle_zoom(int key, int x, int y, t_fractol *fractol);
void			handle_move(int key, t_fractol *fractol);
void			handle_fractale(int key, t_fractol *fractol);

#endif

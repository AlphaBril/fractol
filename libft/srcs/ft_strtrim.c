/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:54 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 17:12:23 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

static int	ft_is_trim(char c)
{
	if (c == ' ' || c == '\t' || c == '\n')
		return (1);
	else
		return (0);
}

static char	*ft_copy(char const *s, int start, int stop)
{
	int		i;
	char	*ret;

	i = 0;
	if (!(ret = (char *)malloc(sizeof(char) * (stop - start) + 2)))
		return (NULL);
	while (start <= stop)
	{
		ret[i] = s[start];
		i++;
		start++;
	}
	ret[i] = '\0';
	return (ret);
}

char		*ft_strtrim(char const *s)
{
	int i;
	int start;
	int stop;

	i = 0;
	if (s)
	{
		while (ft_is_trim(s[i]) == 1)
			i++;
		start = i;
		stop = ft_strlen(s) - 1;
		while (ft_is_trim(s[stop]) == 1)
			stop--;
		if (stop < 1)
			return (ft_strsub(s, 0, 0));
		return (ft_copy(s, start, stop));
	}
	return (NULL);
}

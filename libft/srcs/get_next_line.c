/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:54 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/19 12:56:15 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

static int	ft_slice(char **stc, char **line, int fd, int ret)
{
	int		n;
	char	*tmp;

	if (ft_strchr(stc[fd], '\n') == NULL && ret == 0 && stc[fd][0])
	{
		if (!(*line = ft_strdup(stc[fd])))
			return (-1);
		ft_strdel(&stc[fd]);
		return (1);
	}
	n = (ft_strchr(stc[fd], '\n') - stc[fd]);
	if (!(*line = ft_strsub(stc[fd], 0, n)))
		return (-1);
	if (!(tmp = ft_strdup(&stc[fd][n + 1])))
		return (-1);
	if (stc[fd])
		free(stc[fd]);
	stc[fd] = tmp;
	return (1);
}

int			get_next_line(const int fd, char **line)
{
	int			ret;
	char		buf[BUFF_SIZE + 1];
	static char	*stc[4864];

	ret = 1;
	if (fd < 0 || !line || BUFF_SIZE == 0)
		return (-1);
	while (ret != 0)
	{
		if ((ret = read(fd, buf, BUFF_SIZE)) == -1)
			return (-1);
		buf[ret] = '\0';
		if (!(stc[fd] = ft_strjoin_free(stc[fd], buf, 1)))
			return (-1);
		if (ft_strchr(stc[fd], '\n'))
			return (ft_slice(stc, line, fd, ret));
		if (ft_strchr(stc[fd], '\n') == NULL && ret == 0 && stc[fd][0])
			return (ft_slice(stc, line, fd, ret));
	}
	if (stc[fd])
		ft_strdel(&stc[fd]);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:54 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 17:11:37 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*rahc;

	i = 0;
	rahc = NULL;
	while (s[i])
	{
		if (s[i] == c)
			rahc = (char*)&s[i];
		i++;
	}
	if (c == '\0')
		return ((char*)&s[i]);
	return (rahc);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:51 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 16:53:37 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

static	int	ft_isspace(char c)
{
	return (c == ' ' || c == '\f' || c == '\t' || c == '\n' ||
			c == '\r' || c == '\v');
}

int			ft_atoi(const char *str)
{
	int			i;
	long long	ret;
	int			neg;

	i = 0;
	ret = 0;
	while (ft_isspace(str[i]))
		i++;
	neg = (str[i] == '-') ? -1 : 1;
	(str[i] == '-' || str[i] == '+') ? i++ : 0;
	while (ft_isdigit((int)str[i]))
		ret = ret * 10 + (str[i++] - '0');
	return (ret * neg);
}

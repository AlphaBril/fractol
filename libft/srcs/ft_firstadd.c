/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_firstadd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:51 by fldoucet          #+#    #+#             */
/*   Updated: 2018/12/13 17:09:50 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

void	ft_firstadd(t_list **alst, t_list *new)
{
	t_list *tmp;

	tmp = *alst;
	if (!tmp)
		*alst = ft_lstnew(new->content, new->content_size);
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = ft_lstnew(new->content, new->content_size);
	}
}

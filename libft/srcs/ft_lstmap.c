/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:52 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 16:59:38 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *tmp;
	t_list *mapped;

	tmp = lst;
	if (!(mapped = (t_list *)malloc(sizeof(t_list) * ft_lstsize(lst))))
		return (NULL);
	mapped->content = NULL;
	if (!tmp)
		return (NULL);
	while (tmp)
	{
		ft_firstadd(&mapped, (*f)(tmp));
		tmp = tmp->next;
	}
	return (mapped);
}

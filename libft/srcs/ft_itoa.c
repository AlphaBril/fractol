/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:51 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 16:58:28 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

static char	*ft_calc_itoa(char *ret, unsigned int tmp, int power, int neg)
{
	ret[power + 1] = '\0';
	while (tmp > 0)
	{
		if (neg == -1)
			ret[power] = (tmp % 10) + 48;
		if (neg == 1)
			ret[power - 1] = (tmp % 10) + 48;
		tmp = tmp / 10;
		power--;
	}
	return (ret);
}

char		*ft_itoa(int n)
{
	unsigned int	tmp;
	int				neg;
	int				power;
	char			*ret;

	neg = 1;
	power = 0;
	if (n < 0)
		neg = -1;
	tmp = n * neg;
	while (tmp > 0)
	{
		tmp = tmp / 10;
		power++;
	}
	if (!(ret = malloc(sizeof(char) * (power) + 1)))
		return (0);
	if (neg == -1)
		ret[0] = '-';
	if (n == 0)
		ret[0] = '0';
	else
		ret[power] = '\0';
	tmp = n * neg;
	return (ft_calc_itoa(ret, tmp, power, neg));
}

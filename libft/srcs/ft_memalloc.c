/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:52 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 17:00:35 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

void	*ft_memalloc(size_t size)
{
	void			*all;
	unsigned char	*tmp;
	size_t			i;

	i = 0;
	if (!(all = (void*)malloc(sizeof(void) * size)))
		return (0);
	tmp = all;
	while (i < size)
	{
		tmp[i] = 0;
		i++;
	}
	return (all);
}

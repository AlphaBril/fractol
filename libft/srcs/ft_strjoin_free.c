/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_free.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:54 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 17:48:52 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

static void	ft_thankfull(char *str1, char *str2, int mode)
{
	if (mode == 0)
	{
		if (str1)
			free(str1);
		if (str2)
			free(str2);
	}
	if (mode == 1)
		if (str1)
			free(str1);
	if (mode == 2)
		if (str2)
			free(str2);
}

char		*ft_strjoin_free(char *s1, char *s2, int mode)
{
	int		i;
	int		j;
	int		length;
	char	*rahc;

	i = 0;
	j = 0;
	length = ft_strlen(s1) + ft_strlen(s2);
	if (!(rahc = (char*)malloc(sizeof(char) * length + 1)))
		return (NULL);
	while (s1 && s1[i] != '\0')
	{
		rahc[i] = s1[i];
		i++;
	}
	while (s2 && s2[j] != '\0')
	{
		rahc[i] = s2[j];
		i++;
		j++;
	}
	rahc[i] = '\0';
	ft_thankfull(s1, s2, mode);
	return (rahc);
}

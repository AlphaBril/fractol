/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:52 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 17:02:47 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	size_t			i;
	unsigned char	*tmp1;
	unsigned char	*tmp2;

	i = 0;
	tmp1 = (unsigned char *)src;
	tmp2 = (unsigned char *)dest;
	if (dest > src)
	{
		while (n > 0)
		{
			tmp2[n - 1] = tmp1[n - 1];
			n--;
		}
	}
	else
	{
		while (i < n)
		{
			tmp2[i] = tmp1[i];
			i++;
		}
	}
	return (dest);
}

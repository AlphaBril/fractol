/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/02 13:16:00 by fldoucet          #+#    #+#             */
/*   Updated: 2019/08/14 13:34:27 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
** Listage des touches utiles
** esc : 53 | up : 126 | down : 125 | left : 123 | right : 124 | + : 69 |
** - : 78 | ctrl : 256 | shift : 257 | 1 : 18 | 2 : 19 | 3 : 20 | y : 16
*/

void	draw_fractale(t_fractol *fractol)
{
	if (ft_strcmp(fractol->fractale, "Mandelbrot") == 0)
		fractol->boost == 0 ? mandelbrot(fractol) : mandelbrot_pthread(fractol);
	if (ft_strcmp(fractol->fractale, "Celtic Mandelbrot") == 0)
		fractol->boost == 0 ? c_mandelbrot(fractol)
			: c_mandelbrot_pthread(fractol);
	if (ft_strcmp(fractol->fractale, "Burning Ship") == 0)
		fractol->boost == 0 ? burningship(fractol)
			: burningship_pthread(fractol);
	if (ft_strcmp(fractol->fractale, "Julia") == 0)
		fractol->boost == 0 ? julia(fractol) : julia_pthread(fractol);
	if (ft_strcmp(fractol->fractale, "Chameleon") == 0)
		fractol->boost == 0 ? chameleon(fractol) : chameleon_pthread(fractol);
	if (ft_strcmp(fractol->fractale, "Buddhabrot") == 0)
		buddhabrot(fractol);
	mlx_put_image_to_window(fractol->mlx_ptr, fractol->win_ptr,
		fractol->img_ptr, 0, 0);
	mlx_hook(fractol->win_ptr, 2, (1L << 0), ft_deal_key, fractol);
	mlx_mouse_hook(fractol->win_ptr, handle_mouse, fractol);
	mlx_hook(fractol->win_ptr, 6, (1L << 6), handle_mouse_pos, fractol);
	mlx_loop(fractol->mlx_ptr);
}

int		ft_init_mlx(t_fractol *fractol)
{
	int			bbp;
	int			s_l;
	int			endian;

	fractol->mlx_ptr = mlx_init();
	fractol->win_ptr = mlx_new_window(fractol->mlx_ptr,
		X_MAX, Y_MAX, "FRACTOL");
	fractol->img_ptr = mlx_new_image(fractol->mlx_ptr, X_MAX, Y_MAX);
	fractol->data_ptr = mlx_get_data_addr(fractol->img_ptr,
		&bbp, &s_l, &endian);
	fractol->it_max = 32;
	fractol->stop = 0;
	fractol->clr = 1;
	fractol->boost = 0;
	fractol->y = 0;
	fractol->y_max = 1000;
	fractol->pixels_r = NULL;
	fractol->zoom = 350;
	fractol->x1 = ft_strcmp(fractol->fractale, "Julia") == 0 ? -1.5 : -2.1;
	fractol->x2 = ft_strcmp(fractol->fractale, "Julia") == 0 ? 1.5 : 0.6;
	fractol->y1 = ft_strcmp(fractol->fractale, "Julia") == 0 ? -1.5 : -1.2;
	fractol->y2 = ft_strcmp(fractol->fractale, "Julia") == 0 ? 1.5 : 1.2;
	draw_fractale(fractol);
	return (0);
}

int		ft_deal_key(int key, t_fractol *fractol)
{
	if (key == 65307 || key == 53)
		exit(0);
	if (key == 18 || key == 19 || key == 20 || key == 21
		|| key == 22 || key == 23)
		handle_fractale(key, fractol);
	if (key == 13 || key == 14)
		fractol->clr = key == 13 ? 2 : -2;
	if (key == 32)
		if (fractol->it_max < 1024)
			fractol->it_max *= 2;
	if (key == 34)
		if (fractol->it_max > 32)
			fractol->it_max /= 2;
	if (key == 16)
		fractol->smooth = fractol->smooth == 0 ? 1 : 0;
	if (key == 123 || key == 124 || key == 125 || key == 126)
		handle_move(key, fractol);
	if (key == 12 || key == 38 || key == 40)
		smooth_move_julia(key, fractol);
	if (key == 49)
		fractol->stop = fractol->stop == 0 ? 1 : 0;
	if (key == 11)
		fractol->boost = fractol->boost == 0 ? 1 : 0;
	draw_fractale(fractol);
	return (0);
}

int		main(int ac, char **av)
{
	t_fractol	fractol;

	if (ac == 2 && (!ft_strcmp(av[1], "Mandelbrot")
		|| !ft_strcmp(av[1], "Julia")
		|| !ft_strcmp(av[1], "Mandelbulb")
		|| !ft_strcmp(av[1], "Celtic Mandelbrot")
		|| !ft_strcmp(av[1], "Burning Ship")
		|| !ft_strcmp(av[1], "Chameleon")
		|| !ft_strcmp(av[1], "Buddhabrot")))
	{
		fractol.fractale = av[1];
		ft_init_mlx(&fractol);
	}
	else
	{
		ft_putendl("usage: ./fractol [ Mandelbrot | Julia | Buddhabrot");
		ft_putendl(" | Celtic Mandelbrot | Burning Ship | Chameleon]");
		return (-1);
	}
	return (0);
}

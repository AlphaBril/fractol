/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buddhabrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/02 13:22:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/08/13 17:35:38 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_draw(t_fractol *fractol, int c, int x, int y)
{
	char	*str;

	str = fractol->data_ptr;
	while (++y < Y_MAX)
	{
		x = -1;
		while (++x < X_MAX)
		{
			if ((((x * 4) + 4 * X_MAX * y) + 3) < (X_MAX * Y_MAX * 4) && x > 0
				&& y > 0 && x < X_MAX && y < Y_MAX)
			{
				c = fractol->pixels_r[(x * X_MAX) + y];
				str[((x * 4) + 4 * X_MAX * y) + 2] = (c - 255) < 0 ?
			((c + 255) - ((c - 255) * -1)) * 2 : ((c + 255) - (c - 255)) * 2;
				c = fractol->pixels_g[(x * X_MAX) + y];
				str[((x * 4) + 4 * X_MAX * y) + 1] = (c - 255) < 0 ?
			((c + 255) - ((c - 255) * -1)) * 2 : ((c + 255) - (c - 255)) * 2;
				c = fractol->pixels_b[(x * X_MAX) + y];
				str[((x * 4) + 4 * X_MAX * y) + 0] = (c - 255) < 0 ?
			((c + 255) - ((c - 255) * -1)) * 2 : ((c + 255) - (c - 255)) * 2;
			}
		}
	}
	free(fractol->pixels_r);
}

void		check_and_destroy(t_fractol *fractol, int *pixels,
	t_pixel tmp_pixel[fractol->it_max])
{
	int	i;

	i = 0;
	while (i < fractol->it_max)
	{
		if (tmp_pixel[i].x < X_MAX && tmp_pixel[i].y < Y_MAX
			&& (tmp_pixel[i].x > 0 && tmp_pixel[i].y > 0))
		{
			pixels[tmp_pixel[i].x + (tmp_pixel[i].y * Y_MAX)]++;
			tmp_pixel[i].x = 0;
			tmp_pixel[i].y = 0;
		}
		i++;
	}
}

void		init_pixels(t_fractol *fractol, t_pixel tmp_pixel[fractol->it_max])
{
	int		i;

	if (!(fractol->pixels_r = malloc(sizeof(int) * X_MAX * Y_MAX)))
		exit(0);
	if (!(fractol->pixels_g = malloc(sizeof(int) * X_MAX * Y_MAX)))
		exit(0);
	if (!(fractol->pixels_b = malloc(sizeof(int) * X_MAX * Y_MAX)))
		exit(0);
	i = -1;
	while (++i < X_MAX * Y_MAX)
	{
		fractol->pixels_r[i] = 0;
		fractol->pixels_g[i] = 0;
		fractol->pixels_b[i] = 0;
	}
	i = -1;
	while (++i < fractol->it_max)
	{
		tmp_pixel[i].x = 0;
		tmp_pixel[i].y = 0;
	}
}

void		buddhabrot_bis(int x, int y, t_fractol *fractol,
	t_pixel tmp_pixel[fractol->it_max])
{
	int		i;

	i = 0;
	fractol->tmp = 0;
	fractol->z_r = 0;
	fractol->z_i = 0;
	fractol->c_r = x / (X_MAX / (fractol->x2 - fractol->x1)) + fractol->x1;
	fractol->c_i = y / (Y_MAX / (fractol->y2 - fractol->y1)) + fractol->y1;
	while (fractol->z_r * fractol->z_r + fractol->z_i * fractol->z_i < 4
		&& i < fractol->it_max)
	{
		fractol->tmp = fractol->z_r;
		fractol->z_r = fractol->z_r * fractol->z_r - fractol->z_i
			* fractol->z_i + fractol->c_r;
		fractol->z_i = 2 * fractol->z_i * fractol->tmp + fractol->c_i;
		tmp_pixel[i].x = (fractol->z_r - fractol->x1) * fractol->zoom;
		tmp_pixel[i].y = (fractol->z_i - fractol->y1) * fractol->zoom;
		i++;
	}
	if (i < 300)
		check_and_destroy(fractol, fractol->pixels_r, tmp_pixel);
	if (i < 600)
		check_and_destroy(fractol, fractol->pixels_g, tmp_pixel);
	if (i < 1000)
		check_and_destroy(fractol, fractol->pixels_b, tmp_pixel);
}

void		buddhabrot(t_fractol *fractol)
{
	int		x;
	int		y;
	t_pixel	tmp_pixel[fractol->it_max];

	fractol->zoom = (X_MAX / 3);
	fractol->x1 = -2.1;
	fractol->x2 = 0.6;
	fractol->y1 = -1.5;
	fractol->y2 = 1.5;
	mlx_clear_window(fractol->mlx_ptr, fractol->win_ptr);
	init_pixels(fractol, tmp_pixel);
	y = 0;
	while (y < Y_MAX)
	{
		x = 0;
		while (x < X_MAX)
		{
			buddhabrot_bis(x, y, fractol, tmp_pixel);
			x++;
		}
		y++;
	}
	ft_draw(fractol, 0, 0, 0);
	free(fractol->pixels_g);
	free(fractol->pixels_b);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_hook.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 16:55:07 by fldoucet          #+#    #+#             */
/*   Updated: 2019/08/14 13:34:57 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	handle_zoom(int key, int x, int y, t_fractol *fractol)
{
	double		zoom;

	zoom = (key == 69 ? 1.5 : 0.66);
	fractol->x1 = (x / fractol->zoom + fractol->x1)
		- (x / (fractol->zoom * zoom));
	fractol->y1 = (y / fractol->zoom + fractol->y1)
		- (y / (fractol->zoom * zoom));
	fractol->zoom *= zoom;
	fractol->it_max += (key == 69 ? 1 : -1);
}

void	handle_move(int key, t_fractol *fractol)
{
	if (key == 123)
	{
		fractol->x1 -= 0.1;
		fractol->x2 -= 0.1;
	}
	if (key == 124)
	{
		fractol->x1 += 0.1;
		fractol->x2 += 0.1;
	}
	if (key == 125)
	{
		fractol->y1 += 0.1;
		fractol->y2 += 0.1;
	}
	if (key == 126)
	{
		fractol->y1 -= 0.1;
		fractol->y2 -= 0.1;
	}
}

void	handle_fractale(int key, t_fractol *fractol)
{
	fractol->it_max = 32;
	fractol->zoom = 350;
	if (key == 18)
	{
		fractol->fractale = "Mandelbrot";
		fractol->x1 = -2.1;
		fractol->x2 = 0.6;
		fractol->y1 = -1.2;
		fractol->y2 = 1.2;
	}
	if (key == 19)
	{
		fractol->fractale = "Julia";
		fractol->x1 = -1.5;
		fractol->x2 = 1.5;
		fractol->y1 = -1.5;
		fractol->y2 = 1.5;
		fractol->c_r = 0.1;
		fractol->c_i = 0.6;
	}
	key == 22 ? fractol->fractale = "Buddhabrot" : NULL;
	key == 23 ? fractol->fractale = "Chameleon" : NULL;
	key == 21 ? fractol->fractale = "Celtic Mandelbrot" : NULL;
	key == 20 ? fractol->fractale = "Burning Ship" : NULL;
}

int		handle_mouse_pos(int x, int y, t_fractol *fractol)
{
	if (x > 0 && y > 0 && x < X_MAX && y < Y_MAX && fractol->stop == 0)
	{
		fractol->c_r = x / (X_MAX / (fractol->x2 - fractol->x1)) + fractol->x1;
		fractol->c_i = y / (Y_MAX / (fractol->y2 - fractol->y1)) + fractol->y1;
	}
	draw_fractale(fractol);
	return (0);
}

int		handle_mouse(int key, int x, int y, t_fractol *fractol)
{
	if (key == 1)
		handle_zoom(69, x, y, fractol);
	if (key == 2)
		handle_zoom(0, x, y, fractol);
	if (key == 4)
		handle_zoom(0, x, y, fractol);
	if (key == 5)
		handle_zoom(69, x, y, fractol);
	draw_fractale(fractol);
	return (0);
}

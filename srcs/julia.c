/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/02 13:22:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/08/14 13:33:53 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_draw(int x, int y, double i, t_fractol *fractol)
{
	char	*str;

	str = fractol->data_ptr;
	if (i >= fractol->it_max - 1)
	{
		str[x * 4 + 4 * X_MAX * y + 0] = 0;
		str[x * 4 + 4 * X_MAX * y + 1] = 0;
		str[x * 4 + 4 * X_MAX * y + 2] = 0;
	}
	else
	{
		str[x * 4 + 4 * X_MAX * y + 0] = (give_color((int)(floor(i * 2)) % 512)
				* fractol->clr);
		str[x * 4 + 4 * X_MAX * y + 1] = (give_color((int)(floor(i * 3)) % 512)
				* fractol->clr);
		str[x * 4 + 4 * X_MAX * y + 2] = (give_color((int)(floor(i * 5)) % 512)
				* fractol->clr);
	}
}

void		julia_bis(int x, int y, t_fractol *fractol)
{
	double		i;
	double		z;

	i = 0.0;
	z = 0.0;
	fractol->tmp = 0;
	fractol->z_r = x / fractol->zoom + fractol->x1;
	fractol->z_i = y / fractol->zoom + fractol->y1;
	while (z < 50 && i < fractol->it_max)
	{
		fractol->tmp = fractol->z_r;
		fractol->z_r = fractol->z_r * fractol->z_r - fractol->z_i
			* fractol->z_i + fractol->c_r;
		fractol->z_i = 2 * fractol->z_i * fractol->tmp + fractol->c_i;
		z = fractol->z_r * fractol->z_r + fractol->z_i * fractol->z_i;
		i += 1.0;
	}
	if (i < fractol->it_max - 1 && fractol->smooth == 1)
	{
		i = i + 2 - log(log(z)) / M_LN2;
		i = 8 * sqrt(i);
	}
	ft_draw(x, y, i, fractol);
}

void		julia(t_fractol *fractol)
{
	int	x;
	int	y;
	int	tmp;

	mlx_clear_window(fractol->mlx_ptr, fractol->win_ptr);
	y = 0;
	tmp = fractol->y;
	while (y < Y_MAX)
	{
		x = tmp;
		while (x < fractol->y_max)
		{
			julia_bis(x, y, fractol);
			x++;
		}
		y++;
	}
}

void		julia_pthread(t_fractol *fractol)
{
	int			i;
	t_fractol	tab[100];
	pthread_t	t[100];

	i = 0;
	while (i < 100)
	{
		ft_memcpy((void*)&tab[i], (void*)fractol, sizeof(t_fractol));
		tab[i].y = 10 * i;
		tab[i].y_max = 10 * (i + 1);
		pthread_create(&t[i], NULL, (void*)julia, &tab[i]);
		i++;
	}
	while (i--)
		pthread_join(t[i], NULL);
}

void		smooth_move_julia(int key, t_fractol *fractol)
{
	if (key == 12)
		fractol->clr = 1;
	if (key == 38 && !ft_strcmp(fractol->fractale, "Julia")
			&& fractol->stop == 1)
	{
		fractol->c_r += 0.001;
		fractol->c_i += 0.001;
	}
	if (key == 40 && !ft_strcmp(fractol->fractale, "Julia")
			&& fractol->stop == 1)
	{
		fractol->c_r -= 0.001;
		fractol->c_i -= 0.001;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@stud.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/02 13:22:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/08/14 13:34:02 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int			give_color(int i)
{
	if (i < 128)
		return (128 + i);
	else if (i < 384)
		return (383 - i);
	else
		return (i - 384);
}

static void	ft_draw(int x, int y, double i, t_fractol *fractol)
{
	char	*str;

	str = fractol->data_ptr;
	if (i >= fractol->it_max - 1)
	{
		str[x * 4 + 4 * X_MAX * y + 0] = 0;
		str[x * 4 + 4 * X_MAX * y + 1] = 0;
		str[x * 4 + 4 * X_MAX * y + 2] = 0;
	}
	else
	{
		str[x * 4 + 4 * X_MAX * y + 0] = (give_color((int)(floor(i * 2)) % 512)
				* fractol->clr);
		str[x * 4 + 4 * X_MAX * y + 1] = (give_color((int)(floor(i * 3)) % 512)
				* fractol->clr);
		str[x * 4 + 4 * X_MAX * y + 2] = (give_color((int)(floor(i * 5)) % 512)
				* fractol->clr);
	}
}

void		mandelbrot_bis(int x, int y, t_fractol *f)
{
	double	i;
	double	z;

	i = 0.0;
	z = 0.0;
	f->tmp = 0;
	f->z_r = 0;
	f->z_i = 0;
	f->c_r = x / f->zoom + f->x1;
	f->c_i = y / f->zoom + f->y1;
	while (z < 50 && i < f->it_max)
	{
		f->tmp = f->z_r;
		f->z_r = f->z_r * f->z_r - f->z_i * f->z_i + f->c_r;
		f->z_i = 2 * f->z_i * f->tmp + f->c_i;
		z = f->z_r * f->z_r + f->z_i * f->z_i;
		i += 1.0;
	}
	if (i < f->it_max - 1 && f->smooth == 1)
	{
		i = i + 2 - log(log(z)) / M_LN2;
		i = 8 * sqrt(i);
	}
	ft_draw(x, y, i, f);
}

void		mandelbrot(t_fractol *fractol)
{
	int	x;
	int	y;
	int	tmp;

	mlx_clear_window(fractol->mlx_ptr, fractol->win_ptr);
	y = 0;
	tmp = fractol->y;
	while (y < Y_MAX)
	{
		x = tmp;
		while (x < fractol->y_max)
		{
			mandelbrot_bis(x, y, fractol);
			x++;
		}
		y++;
	}
}

void		mandelbrot_pthread(t_fractol *fractol)
{
	int			i;
	t_fractol	tab[100];
	pthread_t	t[100];

	i = 0;
	while (i < 100)
	{
		ft_memcpy((void*)&tab[i], (void*)fractol, sizeof(t_fractol));
		tab[i].y = 10 * i;
		tab[i].y_max = 10 * (i + 1);
		pthread_create(&t[i], NULL, (void*)mandelbrot, &tab[i]);
		i++;
	}
	while (i--)
		pthread_join(t[i], NULL);
}
